#!/usr/bin/python
from PIL import Image
import os, sys

path = "data_unlabel/bun-bo-hue"
dirs = os.listdir( path )

def resize():
    count = 0
    for item in dirs:
        if os.path.isfile(path+"/"+item):
            try:
                im = Image.open(path+"/"+item)
                if im.mode in ("RGBA", "P"):
                    im = im.convert("RGB")
                imResize = im.resize((400,400), Image.ANTIALIAS)
                imResize.save(f"{path}" + f'/{item}', 'JPEG', quality=90)
                count += 1
            except:
                print(f'Error: {item}')
    print(f'Handle {count} items')


resize()